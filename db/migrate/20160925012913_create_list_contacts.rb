class CreateListContacts < ActiveRecord::Migration
  def change
    create_table :list_contacts do |t|
      t.belongs_to :list, index: true, foreign_key: true
      t.belongs_to :contact, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
