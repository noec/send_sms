class Venbrain
	include HTTParty
	base_uri 'https://sms4geeks.appspot.com/smsgateway'
	OPTION_DEFAULTS = {
    action: 'out',
    username: 'haykerhs',
    password: '1011.11010.111'
  }


	def self.send_multi_sms(list_contacts, sms)
		enviados = 0
		list_contacts.each do |contact|
			enviados += 1 if self.send_one_sms(contact, sms)
		end
		enviados
	end

	def self.send_one_sms(contact, sms)
		options = OPTION_DEFAULTS.merge!({msisdn: contact, msg: sms})
		response = self.get(base_uri, query: options)
		response.response.code == '200'
	end
	# curl "https://sms4geeks.appspot.com/smsgateway?action=out&username=haykerhs&password=1011.11010.111&msisdn=55002709&msg=VenBrain"
end
