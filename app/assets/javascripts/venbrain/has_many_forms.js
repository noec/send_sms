var HasManyForms = Backbone.View.extend({
  el: 'form',
  events: {
    'click .add-has_many': 'add_has_many_row',
    'click .delete-has_many': 'delete_has_many_row'
  },
  add_has_many_row: function(event) {
    event.preventDefault();
    var target = $(event.target),
        has_many_target = $('#' + target.data('target-id')),
        has_many_template = $('#' + target.data('target-id') + '_template .has_many-item').clone(),
        item_number = has_many_target.find('.has_many-item:last').data('item-number') + 1;
        target_id = new Date().getTime();
    if (target.hasClass('disabled')) {
      alert(target.data('alert-message'));
      return 0;
    }
    if (isNaN(item_number)) {
      item_number = 1;
    }
    has_many_template.attr('data-item-number', item_number);
    var has_many_template_id = has_many_template.attr('id').replace('_item-number_', target_id);
    has_many_template.attr('id', has_many_template_id);
    has_many_template.find('a.delete-has_many').data('target-id', has_many_template_id);
    has_many_template.find('div.row div.item-number input').val(item_number);
    has_many_template.find('.template_input').each(function(){
      var input = $(this);
      var input_id = input.data('template-name').replace('_item-number_', target_id);
      var input_name = input.data('template-name').replace('_item-number_', target_id);
      input.attr('id', input_id);
      input.attr('name', input_name);
      input.removeClass('template_input');
    });
    has_many_target.append(has_many_template);
    create_select_with_ajax(has_many_target);
    //$('.autonumeric').autoNumeric('init');
  },
  delete_has_many_row: function(event) {
    var target = $(event.target);
    $('#' + target.data('target-id')).remove();
  }
});

$(document).ready(function(){
  new HasManyForms();
});
