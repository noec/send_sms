window.Venbrain = {
  Models: {},
  Collections: {},
  Views: {},
  Routers: {},
  Listener: {}
};

$(document).ready(function(){
  create_select_with_ajax();
});

jQuery.fn.extend({
  exists: function() { return this.length > 0; }
});

function create_select_with_ajax(self) {
  if(!self) {
    self = $('body');
  }
  chosen = self.find('select[class=chosen]');
  chosen2 = $("select[data-select-type='chosen']:not(.select2-hidden-accessible):not(.template_input)");
  if (self.find('select[class=select_ajax]').length > 0) {
    chosen_ajax = self.find('select[class=select_ajax]')
  } else {
    chosen_ajax = $("select[data-select-type='chosen-ajax']:not(.select2-hidden-accessible):not(.template_input)");
  }
  // el fn.select.amd se agrego para poder incluir el select2 dentro de contenedor y no
  // en el body que es como lo hace por defecto
  $.fn.select2.amd.require([
    "select2/utils",
    "select2/dropdown",
    "select2/dropdown/attachContainer",
    "select2/dropdown/search",
    'select2/dropdown/closeOnSelect',
  ], function (Utils, DropdownAdapter, AttachContainer, DropdownSearch, CloseOnSelect) {
    var CustomAdapter = Utils.Decorate(
      Utils.Decorate(
        Utils.Decorate(DropdownAdapter, DropdownSearch),
        AttachContainer),
      CloseOnSelect
    );
    if (chosen.length > 0) {
      chosen.select2({
        dropdownAdapter: CustomAdapter
      });
    }
    if (chosen2.length > 0) {
      chosen2.select2({
        dropdownAdapter: CustomAdapter
      });
    }
    if(chosen_ajax.length > 0) {
      chosen_ajax.select2({
        minimumInputLength: 3,
        language: {
          inputTooShort: function () {
            return "Por favor, ingrese 3 caracteres o mas";
          }
        },
        ajax: {
          dataType: "json",
          type: $(chosen_ajax).data('type-ajax') || 'GET',
          data: function (data) {
            return {
              term: data.term,
            };
          },
          processResults: function (data, page) {
            return {
              results: $.map(data, function (field, i) {
                return {
                  id: field.id,
                  text: field.display_name
                };
              })
            };
          }
        },
        dropdownAdapter: CustomAdapter
      });
    }
  });
}
