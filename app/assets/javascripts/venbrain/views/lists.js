Venbrain.Views.List = Backbone.View.extend({
	el: 'body',
	events: {
		'change .contact_id': 'updateNombre'
	},
	updateNombre: function(event){
		var has_many_row = $(event.target).parents('.has_many-item'),
			contact_id = $(event.target).val(),
			nombre_input = has_many_row.find('.nombre');
		$.ajax({
			url: 'contact/nombre',
			dataType: 'JSON',
			type: 'GET',
			data: {
			  contact_id: contact_id
			}
		}).done(function(data){
			nombre_input.val(data.nombre);
		});
	},
});

$(document).ready(function(){
	new Venbrain.Views.List();
});