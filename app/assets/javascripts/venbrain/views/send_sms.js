Venbrain.Views.SendSms = Backbone.View.extend({
	el: 'body',
	events: {
		'change #agrupar': 'showHasMany',
		'change .contact_id': 'updateNombre',
		'change .list_id': 'updateDescripcion'
	},
	showHasMany: function(event) {
		var val_agrupar = $(event.target).val();
		const LISTA = 1 , CONTACTO = 2;
		if(val_agrupar==CONTACTO){
			this.mostrarManyContacts();
		}else{
			if(val_agrupar==LISTA){
				this.mostrarManyLists();
			}else{
				$('#agrupar').val(LISTA).trigger('change');
			}
		}
	},
	mostrarManyContacts: function() {
		$('#list_contacts_items').find('.has_many-item').remove();
		$('#partial_list_contact').addClass('hide');
		$('#partial_contacts').removeClass('hide');
	},
	mostrarManyLists: function() {
		$('#contacts_items').find('.has_many-item').remove();
		$('#partial_contacts').addClass('hide');
		$('#partial_list_contact').removeClass('hide');
	},
	updateNombre: function(event){
		var has_many_row = $(event.target).parents('.has_many-item'),
			contact_id = $(event.target).val(),
			nombre_input = has_many_row.find('.nombre');
		$.ajax({
			url: 'contact/nombre',
			dataType: 'JSON',
			type: 'GET',
			data: {
			  contact_id: contact_id
			}
		}).done(function(data){
			nombre_input.val(data.nombre);
		});
	},
	updateDescripcion: function(event){
		var has_many_row = $(event.target).parents('.has_many-item'),
			list_id = $(event.target).val(),
			descripcion_input = has_many_row.find('.descripcion');
		$.ajax({
			url: 'list/descripcion',
			dataType: 'JSON',
			type: 'GET',
			data: {
			  list_id: list_id
			}
		}).done(function(data){
			descripcion_input.val(data.descripcion);
		});
	}
});

$(document).ready(function(){
	new Venbrain.Views.SendSms();
});