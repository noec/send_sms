json.array!(@lists) do |list|
  json.extract! list, :id, :nombre, :descripcion
  json.url list_url(list, format: :json)
end
