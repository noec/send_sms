class ListContact < ActiveRecord::Base
  belongs_to :list
  belongs_to :contact
  LISTA = 1
  CONTACTO = 2

  def self.agrupar
  	[{id: self::LISTA, text: 'Lista de Contactos'}, {id: self::CONTACTO, text: 'Contactos Individuales'}]
  end
end
