class List < ActiveRecord::Base
	validates :nombre, presence: true
	has_many :list_contacts, dependent: :destroy
  	accepts_nested_attributes_for :list_contacts, allow_destroy: true
end
