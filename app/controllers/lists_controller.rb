class ListsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_list, only: [:new, :show, :edit, :update, :destroy]

  # GET /lists
  # GET /lists.json
  def index
    @lists = List.all
  end

  # GET /lists/1
  # GET /lists/1.json
  def show
  end

  # GET /lists/new
  def new
    render template: 'lists/form', layout: 'layouts/application'
  end

  # GET /lists/1/edit
  def edit
    render template: 'lists/form', layout: 'layouts/application'
  end

  # POST /lists
  # POST /lists.json
  def create
    @list = List.new(list_params)

    respond_to do |format|
      if @list.save
        format.html { redirect_to @list, notice: 'List was successfully created.' }
        format.json { render :show, status: :created, location: @list }
      else
        format.html { render template: 'lists/form', layout: 'layouts/application' }
        format.json { render json: @list.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /lists/1
  # PATCH/PUT /lists/1.json
  def update
    respond_to do |format|
      if @list.update(list_params)
        format.html { redirect_to @list, notice: 'List was successfully updated.' }
        format.json { render :show, status: :ok, location: @list }
      else
        format.html { render template: 'lists/form', layout: 'layouts/application' }
        format.json { render json: @list.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /lists/1
  # DELETE /lists/1.json
  def destroy
    @list.destroy
    respond_to do |format|
      format.html { redirect_to lists_url, notice: 'List was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def descripcion
    descripcion = '---'
    if params[:list_id].present? && params[:list_id].to_i > 0
      descripcion = List.find(params[:list_id].to_i).try(:descripcion) || '---'
    end
    render json: {descripcion: descripcion}
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_list
      if params[:id].present?
        @list = List.find(params[:id])
        @url = list_path(@list)
      else
        @list = List.new
        @url = lists_path
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def list_params
      params.require(:list).permit(:nombre, :descripcion, list_contacts_attributes: [:id, :list_id, :contact_id, :_destroy])
    end
end
