class ContactsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_contact, only: [:new, :show, :edit, :update, :destroy]

  # GET /contacts
  # GET /contacts.json
  def index
    @contacts = Contact.all
  end

  # GET /contacts/1
  # GET /contacts/1.json
  def show
  end

  # GET /contacts/new
  def new
    render template: 'contacts/form', layout: 'layouts/application'
  end

  # GET /contacts/1/edit
  def edit
    render template: 'contacts/form', layout: 'layouts/application'
  end

  # POST /contacts
  # POST /contacts.json
  def create
    @contact = Contact.new(contact_params)

    respond_to do |format|
      if @contact.save
        format.html { redirect_to @contact, notice: 'Contact was successfully created.' }
        format.json { render :show, status: :created, location: @contact }
      else
        format.html { render template: 'contacts/form', layout: 'layouts/application' }
        format.json { render json: @contact.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /contacts/1
  # PATCH/PUT /contacts/1.json
  def update
    respond_to do |format|
      if @contact.update(contact_params)
        format.html { redirect_to @contact, notice: 'Contact was successfully updated.' }
        format.json { render :show, status: :ok, location: @contact }
      else
        format.html { render template: 'contacts/form', layout: 'layouts/application' }
        format.json { render json: @contact.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /contacts/1
  # DELETE /contacts/1.json
  def destroy
    @contact.destroy
    respond_to do |format|
      format.html { redirect_to contacts_url, notice: 'Contact was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def nombre
    nombre = '---'
    if params[:contact_id].present? && params[:contact_id].to_i > 0
      nombre = Contact.find(params[:contact_id].to_i).try(:nombre) || '---'
    end
    render json: {nombre: nombre}
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contact
      if params[:id].present?
        @contact = Contact.find(params[:id])
        @url = contact_path(@contact)
      else
        @contact = Contact.new
        @url = contacts_path
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def contact_params
      params.require(:contact).permit(:nombre, :numero)
    end
end
