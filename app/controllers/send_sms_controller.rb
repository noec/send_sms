class SendSmsController < ApplicationController
	require Rails.root.join('lib', 'venbrain.rb').to_s

	before_action :authenticate_user!, except: :index

	def index
		render template: 'send_sms/form', layout: 'layouts/application'
	end

	def send_sms
		contacts = []
		case params[:agrupar].to_i
		when ListContact::LISTA
			if params[:list_contacts].present?
				params[:list_contacts].each do |param_list|
					if List.exists?(param_list.second[:list_id].to_i)
						contacts += List.find(param_list.second[:list_id].to_i).list_contacts.joins(:contact).pluck(:numero)
					end
				end
			end
		when ListContact::CONTACTO
			if params[:contacts].present?
				params[:contacts].each do |params_contact|
					if Contact.exists?(params_contact.second[:contact_id].to_i)
						contacts << Contact.find(params_contact.second[:contact_id].to_i).try(:numero)
					end
				end
			end
		end
		contacts = contacts.compact.uniq
		mensaje = params[:mensaje]
		if mensaje.blank?
			flash[:alert] = 'El mensaje se encuentra vacio, agregue un mensaje e intente nuevamente'
			render template: 'send_sms/form', layout: 'layouts/application'
		elsif contacts.blank?
			flash[:alert] = 'No existen numero de contactos en los datos, revise sus datos e intente nuevamente'
			render template: 'send_sms/form', layout: 'layouts/application'
		else
			enviados = Venbrain.send('send_multi_sms', contacts, mensaje)
			flash[:notice] = "Se envio el mensaje a #{enviados}/#{contacts.count} contactos"
			redirect_to action: :index
		end
	end
end
