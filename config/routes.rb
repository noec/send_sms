Rails.application.routes.draw do
  devise_for :users
  root to: "home#index"

  resources :contacts
  resources :lists do
    get 'contact/nombre', to: 'contacts#nombre'
    collection do
      get 'contact/nombre', to: 'contacts#nombre'
    end
  end
  match 'contact/nombre', to: 'contacts#nombre', via: :get
  get 'send_sms/index', to: 'send_sms#index'
  get 'send_sms/send_sms', to: 'send_sms#send_sms'
  get 'send_sms/contact/nombre', to: 'contacts#nombre'
  get 'send_sms/list/descripcion', to: 'lists#descripcion'
end
